<div class="alert alert-{$mensage.type}{if $mensage.dismissible} alert-dismissible{/if}" role="alert">
    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    <h4>Ooops! parace que houve um erro...</h4>
    {$mensage.value}
</div>
