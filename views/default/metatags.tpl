{* MetaTags - Para indexar melhor no Google - Editar os campos que forem necessários *}
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="../img/icone.ico" />
<meta name="author" content="Catânia Studio" />
<meta name="Publisher" content="Catânia Studio" />
<meta name="owner" content="Werner Calçados" />
<meta name="copyright" content="Boutique Werner Online © 2015" />
<meta name="description" content="{$description}" />
<meta name="robots" content="{if isset($metaRobots)}{$metaRobots}{else}index,follow{/if}" />
<meta name="rating" content="geral" />
<link rel="canonical" href="https://wernercalcadosonline.com.br/" />
<meta http-equiv="expires" content="" />
<meta http-equiv="content-language" content="Português" />
<meta http-equiv="vw96.objecttype" content="Internet" />

<meta property="og:type" content="website" />
<meta property="og:url" content="https://wernercalcadosonline.com.br/{$route}/{$params|implode:'/'}" />
<meta property="og:title" content="Boutique Werner Online - {$pageTitle}" />
<meta property="og:description" content="{$description}" />
<meta property="og:image" content="https://wernercalcadosonline.com.br/img/facebook.png" />
<meta property="og:site_name" content="Boutique Werner Online" />
<meta property="og:locale" content="pt_BR" />