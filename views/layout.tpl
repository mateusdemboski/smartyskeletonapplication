<!DOCTYPE html>
<html>
<head>
    {include file="../views/default/metatags.tpl"}
    <title>{$pageTitle}</title>
    <!-- Bootstrap 3 -->
    <link type="text/css" rel="stylesheet" href="{$path}/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Custom CSS -->
    <link type="text/css" rel="stylesheet" href="{$path}/style.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

    {include file=$mid}

    <!-- jQuery 1 (necessary for Bootstrap's JavaScript plugins) -->
    <script type="text/javascript" src="{$path}/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3 -->
    <script type="text/javascript" src="{$path}/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Custom JS -->
	<script type="text/javascript" src="{$path}/js/modernizr.min.js"></script>
	<script type="text/javascript" src="{$path}/js/functions.jquery.js"></script>
</body>
</html>
