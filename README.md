SmartySkeletonApplication
=========================

Welcome to my Smarty Skeleton! =)

Constants
---------

* `DS`: Acronym of `DIRECTORY_SEPARATOR`
* `CORE_DIR`:  ...
* `PAGES_DIR`:  ...
* `VENDOR_DIR`: ...

Reserved variables
------------------

### In Smarty!

* Default reserved variables in Smarty (view [{$smarty} reserved variable](http://www.smarty.net/docs/en/language.variables.smarty.tpl));
* The `{$path}` Variable: this is used for get the complements of the current url. Exemple: for 'http://mysite.com/site/' the `{$path}` variable return `/site`;
* The `{$pageTitle}` Variable: Define a title for current page. To change the value of this variable, in php use `$smarty->assign('pageTitle', 'title here');`;

### In PHP

* `$configurations` : ...
* `$connection` : ...
* `$params` : ...
* `$displayTpl` : change name for dafult TPL display. Exemple: for file 'home.php', default  TPL name is 'home.tpl', but can changed using in PHP ` $displayTpl = 'index' `, now PHP go display the view (tpl) 'index.tpl' for home.php page;
