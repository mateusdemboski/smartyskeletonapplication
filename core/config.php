<?php
$configurations = [
    'database' => [
        'driver'   => 'pdo_mysql',
        'host'     => 'localhost',
        'user'     => 'root',
        'port'     => 3306,
        'password' => 'alfazema12',
        'dbname'   => 'mysql',
        'charset'  => 'utf8',
    ],
    'password' => [
        'cost' => 8,
        'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM)
    ],
];
