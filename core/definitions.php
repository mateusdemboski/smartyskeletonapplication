<?php
/**
 * @author Mateus Demboski <mateus@hospedasul.com>
 * @since 04/04/14
 */

if(!defined('DIRECTORY_ROOT')) define('DIRECTORY_ROOT',DS.trim(dirname(dirname(__FILE__)),DS));
if(!defined('CORE_DIR')) define('CORE_DIR', DIRECTORY_ROOT.DS.'core');
if(!defined('PAGES_DIR')) define('PAGES_DIR', DIRECTORY_ROOT.DS.'pages');
if(!defined('VENDOR_DIR')) define('VENDOR_DIR', DIRECTORY_ROOT.DS.'vendor');
