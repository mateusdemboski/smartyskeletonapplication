<?php
/**
 * @author Mateus Demboski <mateus@hospedasul.com>
 * @since 04/04/14
 */
require(CORE_DIR.DS.'config.php');
require(CORE_DIR.DS.'functions.php');
if(file_exists(VENDOR_DIR.DS.'autoload.php')) require(VENDOR_DIR.DS.'autoload.php');
else die('You have already installed the dependencies with composer?');

//Set the connection to Database
$dbconfig = new \Doctrine\DBAL\Configuration();
$database = \Doctrine\DBAL\DriverManager::getConnection($configurations['database'], $dbconfig);

//Set configurations for Smarty Template Engine
$smarty = new Smarty();
$smarty->setTemplateDir(DIRECTORY_ROOT.DS.'views'.DS);
$smarty->setCompileDir (DIRECTORY_ROOT.DS.'tmp'.DS.'templates_c'.DS);
$smarty->setConfigDir  (CORE_DIR.DS.'smarty_configs'.DS);
$smarty->setCacheDir   (DIRECTORY_ROOT.DS.'tmp'.DS.'cache'.DS);

//Set routing and párams
$route = new Routes('pages', trim(parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH),'/\\'));
$route->run();

$path = $route->getPathForURL();
$smarty->assign('path', $path);

$pageFile = DIRECTORY_ROOT.DS.$route->getPathForInclude();

$params = (array) $route->getParamsFromURL();

//rename the original variable to a more friendly
$action = !empty($params[0]) ? $params[0] : '';

//verify and include the requested page
if(file_exists($pageFile)){

    include($pageFile);
    $smarty->assign('route',  $route->getFileName());
    $smarty->assign('action', $action);
    $smarty->assign('params', $params);
    if(isset($displayTpl))$smarty->assign('mid','../views/' . $displayTpl . '.tpl');
    else $smarty->assign('mid','../views/' . $route->getFileName() . '.tpl');
}else{
    header("HTTP/1.0 404 Not Found");
    $smarty->assign('mid', '../views/404.tpl');
}

$smarty->display('layout.tpl');